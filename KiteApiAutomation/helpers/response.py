#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 15 15:53:57 2018

@author: aditya
"""


from helpers.status import Status

class Response():
    def __init__(self, return_obj, status_code):
        self.obj = return_obj
        self.status = Status(status_code)