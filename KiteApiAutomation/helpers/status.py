#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 15 15:48:59 2018

@author: aditya
"""

class Status():
    status_codes = {
                -1 : "Default. Status not initialized",
                200: "Successfully executed task",
                500: "Error while executing the task. Check dump for details"
                }
    def __init__(self, status_code):        
        self.code = status_code
        self.description = self.status_codes[status_code]
        
        