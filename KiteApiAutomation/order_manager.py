#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 15 11:56:27 2018

@author: aditya
"""

from threading import Thread
import openpyxl as data_handler



#import datetime
#import unicodecsv as csv


#from threading import Thread
import pandas as pd
import numpy as np
from stockstats import *
from time import sleep
import logging
from p2vmap import p2v_map as p2v_map
import requests


    
class Order_Manager(Thread):
    def __init__(self, olhc_queue, filename, config_filename, expected_values_sheetname):
        Thread.__init__(self, name="Order Manager")
        self.olhc_queue = olhc_queue
        #self.KITE = KITE
        self.stock_dataframe = {}
        #Indexes filename ... tobe named
        self.filename = filename
        
        self.wb = data_handler.load_workbook(config_filename)
        #Hardcoded
        self.stock_info_sheet = self.wb["Sheet1"]
        self.config_filename_handler = self.wb[expected_values_sheetname]
        self.olhcdata_store = []
        logging.info('[__init__] Order Manager initialized successfully')
    
    def send_simple_message(self, msg, subject):
        return requests.post(
            "https://api.mailgun.net/v3/sandbox0d285dfc7cec4fdbbea3deb967abf2bf.mailgun.org/messages",
            auth=("api", "key-97985787e1dc0321ba7df0bbcb7d4c81"),
            data={"from": "Mailgun Sandbox <postmaster@sandbox0d285dfc7cec4fdbbea3deb967abf2bf.mailgun.org>",
                  "to": self.config_filename_handler[p2v_map['email']].value,
                  "subject": subject,
                  "text": msg})
        
    def chop(self, stocks_df, period):
        logging.debug('[chop] Starting to compute chop index')
        chop_list = []
        if stocks_df.shape[0] < period:
            logging.debug('[chop] chop index computation not possible at this time. return null list')
            return []
        else:
            for index in range(0, stocks_df.shape[0]):
                if index >= period:
                    sum_atr = np.sum(stocks_df["atr_1"].loc[index:index + period])
                    max_high = np.max(stocks_df["high"].loc[index:index + period])
                    min_low = np.min(stocks_df["low"].loc[index:index + period])
                    chop = np.log10(sum_atr/(max_high-min_low)) / np.log(period)
                    chop_list.append(chop)
                else:
                    chop_list.append(0)
            logging.debug('[chop] Completed valid chop index computation')
            return chop_list
        
    
    def calculate_expected_indicators(self):
        logging.debug('[calculate_expected_indicators] Starting computation')
        expected_indicators = {'buy' : {}, 'sell' : {}}
        
        expected_indicators['buy']['rsi'] = float(self.config_filename_handler[p2v_map['buy_rsi']].value) if self.config_filename_handler[p2v_map['buy_rsi']].value != "" else int(-1)
        expected_indicators['buy']['adx'] = float(self.config_filename_handler[p2v_map['buy_adx']].value) if self.config_filename_handler[p2v_map['buy_adx']].value != "" else -1 
        expected_indicators['buy']['chop'] = float(self.config_filename_handler[p2v_map['buy_chop']].value) if self.config_filename_handler[p2v_map['buy_chop']].value != "" else -1
        expected_indicators['buy']['divergence'] = float(self.config_filename_handler[p2v_map['buy_divergence']].value) if self.config_filename_handler[p2v_map['buy_divergence']].value != "" else -1
        
        expected_indicators['sell']['rsi'] = float(self.config_filename_handler[p2v_map['sell_rsi']].value) if self.config_filename_handler[p2v_map['sell_rsi']].value != "" else -1
        expected_indicators['sell']['adx'] = float(self.config_filename_handler[p2v_map['sell_adx']].value) if self.config_filename_handler[p2v_map['sell_adx']].value != "" else -1 
        expected_indicators['sell']['chop'] = float(self.config_filename_handler[p2v_map['sell_chop']].value) if self.config_filename_handler[p2v_map['sell_chop']].value != "" else -1
        expected_indicators['sell']['divergence'] = float(self.config_filename_handler[p2v_map['sell_divergence']].value) if self.config_filename_handler[p2v_map['sell_divergence']].value != "" else -1
        
        logging.debug('[calculate_expected_indicators] Computation complete')
        return expected_indicators

    def run(self):
        sleep(30) # Time in seconds.
        logging.info('[run] Starting Order Manager')
        expected_indicators = self.calculate_expected_indicators()
        while True:
            #currently acts as a trigger. Data not being used anywhere
            data = self.olhc_queue.get()
            logging.debug('[run] Pulled latest data from OLHC_queue')
            
            df = pd.read_csv(filepath_or_buffer=self.filename + ".csv")
            if df.shape[0] >= 13:
                logging.debug('[run] Starting index computation. %s olhc values available', str(df.shape[0]))
                self.stock_dataframe = StockDataFrame.retype(df)
                #compute all the indexes of these values
                s_df = pd.DataFrame()
                s_df['open'] = self.stock_dataframe['open']
                s_df['close'] = self.stock_dataframe['close']
                s_df['high'] = self.stock_dataframe['high']
                s_df['low'] = self.stock_dataframe['low']
                s_df['rsi_12'] = self.stock_dataframe['rsi_12']
                s_df['adx'] = self.stock_dataframe['adx']
                s_df['macd'] = self.stock_dataframe['macd']
                s_df['macds'] = self.stock_dataframe['macds']
                s_df['tr'] = self.stock_dataframe['tr']
                s_df['atr_1'] = self.stock_dataframe['atr_1']
                s_df['chop'] = pd.Series(self.chop(s_df, 14))
                s_df['timestamp'] = self.stock_dataframe['end_timestamp']
                logging.debug('[run] Index computation complete')
                s_df.to_csv(path_or_buf = self.filename + "_Indexes.csv")
                logging.info('[run] Indexes file updated with latest indexes. Location: %s', str(self.filename + "_Indexes.csv"))
                df_tail = s_df.tail(1)
                latest_indicators = {
                        'rsi' : float(df_tail['rsi_12']),
                        'macd' : float(df_tail['macd']),
                        'signal' : float(df_tail['macds']),
                        'adx' : float(df_tail['adx']),
                        'chop' : float(df_tail['chop']),
                        }
                #Decide order execution. Execute the order
                self.decide_and_execute(latest_indicators, expected_indicators)
                logging.info('[run] Decision made.')
            else:
                logging.info('[run] Cannot compute indexes yet. Atleast 13 olhc values needed. Only %s available', str(df.shape[0]))
            
            
    
    def is_ready_to_buy(self, actual_indicators, expected_indicators):
        logging.debug('[is_ready_to_buy] Entered for computation')
        rsi_divergence = actual_indicators['rsi'] / expected_indicators['buy']['divergence']
        chop_divergence = actual_indicators['chop'] / expected_indicators['buy']['divergence']
        adx_divergence = actual_indicators['adx'] / expected_indicators['buy']['divergence']
        if ((expected_indicators['buy']['rsi'] + rsi_divergence) >= actual_indicators['rsi']  >= (expected_indicators['buy']['rsi'] - rsi_divergence)):
            logging.debug('[is_ready_to_buy] RSI in range')
            if ((expected_indicators['buy']['adx'] + adx_divergence) >= actual_indicators['adx']  >= (expected_indicators['buy']['adx'] - adx_divergence)):
                logging.debug('[is_ready_to_buy] ADX in range')
                if ((expected_indicators['buy']['chop'] + chop_divergence) >= actual_indicators['chop']  >= (expected_indicators['buy']['chop'] - chop_divergence)):
                    #Potential Buy Signal : http://www.onlinetradingconcepts.com/TechnicalAnalysis/MACD2.html
                    logging.debug('[is_ready_to_buy] CHOP in range')
                    if (actual_indicators['macd']  > actual_indicators['signal']):
                        logging.debug('[is_ready_to_buy] MACD is above signal line')
                        logging.info('[is_ready_to_buy] Potential Buy signal found')
                        return True
        logging.info('[is_ready_to_buy] Potential Buy signal not found')
        return False
        
    def is_ready_to_sell(self, actual_indicators, expected_indicators):
        logging.debug('[is_ready_to_sell] Entered for computation')
        rsi_divergence = actual_indicators['rsi'] / expected_indicators['sell']['divergence']
        chop_divergence = actual_indicators['chop'] / expected_indicators['sell']['divergence']
        adx_divergence = actual_indicators['adx'] / expected_indicators['sell']['divergence']
        if ((expected_indicators['sell']['rsi'] + rsi_divergence) >= actual_indicators['rsi']  >= (expected_indicators['sell']['rsi'] - rsi_divergence)):
            logging.debug('[is_ready_to_sell] RSI in range')
            if ((expected_indicators['sell']['adx'] + adx_divergence) >= actual_indicators['adx']  >= (expected_indicators['sell']['adx'] - adx_divergence)):
                logging.debug('[is_ready_to_sell] ADX in range')
                if ((expected_indicators['sell']['chop'] + chop_divergence) >= actual_indicators['chop']  >= (expected_indicators['sell']['chop'] - chop_divergence)):
                    #Potential sell signal : http://www.onlinetradingconcepts.com/TechnicalAnalysis/MACD2.html
                    logging.debug('[is_ready_to_sell] CHOP in range')
                    if (actual_indicators['macd']  < actual_indicators['signal']):
                        logging.debug('[is_ready_to_sell] MACD is below the signal line')
                        logging.info('[is_ready_to_sell] Potential Sell signal not found')
                        return True
        logging.info('[is_ready_to_sell] Potential Sell signal not found')
        return False
    
    def decide_and_execute(self, actual_indicators, expected_indicators):
        logging.debug('[decide_and_execute] Entering computation')
        if (self.stock_info_sheet[p2v_map['position_taken']].value.lower() == "yes"):
            if (self.stock_info_sheet[p2v_map['position_liquidated']].value.lower() == "yes"):
                #Second buy condition
                logging.debug('[decide_and_execute] This is a second buy condition')
                if self.is_ready_to_buy(actual_indicators, expected_indicators):
                    logging.info('[decide_and_execute] Conditions for buy found')
                    #self.execute_cover_order(self, KITE, order)
                    
                else:
                    logging.info('[decide_and_execute] Conditions for buy not found')
                    
            else:
                #first sell
                logging.debug('[decide_and_execute] This is a  sell condition')
                if self.is_ready_to_sell(actual_indicators, expected_indicators):
                    logging.info('[decide_and_execute] Conditions for sell found')
                    #self.execute_market_order(self, KITE, order)
                else:
                    logging.info('[decide_and_execute] Conditions for sell not found')
        else:
            #First buy condition
            logging.debug('[decide_and_execute] This is a first buy condition')
            if self.is_ready_to_buy(actual_indicators, expected_indicators):
                logging.info('[decide_and_execute] Conditions for buy found')
                #self.execute_cover_order(self, KITE, order)
            else:
                logging.info('[decide_and_execute] Conditions for buy not met yet')
            
        
        
    def execute_cover_order(self, KITE, order):
        logging.debug('[execute_cover_order] Placing the order')
        try:
            order_id = KITE.place_order(exchange=KITE.EXCHANGE_NSE,
                                        transaction_type=KITE.TRANSACTION_TYPE_BUY,
                                        quantity=order.share.quantity,
                                        variety=KITE.VARIETY_CO,
                                        order_type=KITE.ORDER_TYPE_MARKET,
                                        product=KITE.PRODUCT_MIS)
            
            logging.info('[execute_cover_order] %s', "Order placed. ID is: {}".format(order_id))
            #Set position taken to yes and position liquidated to No
            self.stock_info_sheet[p2v_map['position_taken']].value = "yes"
            self.stock_info_sheet[p2v_map['position_liquidated']].value = "no"
            self.wb.save(self.config_filename)
            logging.debug('[execute_cover_order] Position taken and liquidated updated')
            msg = "A cover order is placed. ID is: {}".format(order_id)
            self.send_simple_message(msg, "Cover order placed successfully.")
        except Exception as e:
            logging.info('[execute_cover_order] %s', "Order placement failed: {}".format(e.message))
        return order_id


    def execute_market_order(self, KITE, order):
        try:
            logging.debug('[execute_market_order] Sending market order to exchange')
            order_id = KITE.order_place(exchange=KITE.EXCHANGE_NSE,
                                        transaction_type=KITE.TRANSACTION_TYPE_SELL,
                                        quantity=order.share.quantity,
                                        validity="DAY",
                                        variety=KITE.VARIETY_REGULAR,
                                        order_type=KITE.ORDER_TYPE_MARKET,
                                        product=KITE.PRODUCT_MIS)

            logging.info('[execute_market_order] Order placed. ID is %s', str(order_id))
            response=KITE.orders(order_id)
            logging.debug('[execute_market_order] Response : %s ', response)
            #Set position liquidated to yes
            self.stock_info_sheet[p2v_map['position_liquidated']].value = "yes"
            self.wb.save(self.config_filename)
            logging.debug('[execute_market_order] Position liquidated updated successfully')
            msg = "Market Order is placed. ID is: {}".format(order_id)
            self.send_simple_message(msg, "Market order placed successfully")
            
        except:
            logging.info('[execute_market_order] Market Order placement failed due to network error.')
