#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  6 00:33:49 2018

@author: aditya
"""

p2v_map = {
        'buy_rsi': 'B2',
        'buy_adx': 'B5',
        'buy_chop': 'B4',
        'buy_divergence': 'B6',
        'sell_rsi': 'D2',
        'sell_adx': 'D5',
        'sell_chop': 'D4',
        'sell_divergence': 'D6',
        'position_taken': 'I2',
        'position_liquidated': 'J2',
        'email' : 'A2',
        'secret': 'O2',
        'api_key': 'N2',
        'order_id': 'M2',
        'access_token': 'L2',
        'share_quantity': 'C2',
        'stop_loss_units':'E2',
        'script_absolute_move' : 'F2',
        'time_of_order_execution':'G2',
        'force_square_off_time': 'H2',
        'p_and_l': 'K2',
        'instrument_name': 'B2'
        }