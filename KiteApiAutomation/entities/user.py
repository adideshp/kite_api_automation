#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 15 14:00:11 2018

@author: aditya
"""

__package__ = None


from helpers.response import Response


class User():
    def __init__(self):
        self.personal_info = {'id' : '-1'}
        self.access_info = {
                'email' : "",
                'secret' : "",
                'api_key' : ""
                }
        self.access_token = "" 


    def get_access_info(self):
        return Response(return_obj = self.access_info, status_code=200)
        
    def set_access_info(self, email, secret, api_key):
        self.access_info['email'] = email 
        self.access_info['secret'] = secret
        self.access_info['api_key'] =api_key
        return Response(return_obj = True, status_code=200)
        
    def get_access_token(self):
        return Response(return_obj = self.access_token, status_code=200)
        
    def set_access_token(self, access_token):
        if access_token == '':
            return Response(return_obj = False, status_code=500)
        else:
            self.access_token = access_token
            return Response(return_obj = True, status_code=200)