#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 15 14:06:51 2018

@author: aditya
"""
__package__ = None


class Share():
    def __init__(self):
        self.olhcv_dataframe = -1
        self.rsi = -1
        self.adx = -1
        self.macd = -1
        self.chop = -1
        self.quantity = -1
        self.stop_loss_units = -1
        self.script_absolute_move = -1
        self.time_of_order_execution = -1
        self.force_square_off_time = -1
        self.position_taken = False
        self.position_liquidated = False
        self.p_and_l = -1
        self.last_updated = -1
        self.candlestick_timeframe = 10
        self.instrument = "None"
    
    
    #Update this function
    def get_olhcv(self):
        return True
        
    #Update this function
    def put_olhcv(self, o, l, h, c, v):
        self.o = o
        self.l = l
        self.h = h
        self.c = c
        self.v = v
        
    def calcute_rsi(self):
        return self.rsi
        
    def calculate_adx(self):
        return self.adx
        
    def calculate_macd(self):
        return self.macd
    
    def calculate_chop(self):
        return self.chop
        
    def set_position_taken(self):
        return True
        
    def set_position_liquidated(self):
        return True

    
        