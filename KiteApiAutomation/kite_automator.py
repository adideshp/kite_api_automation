#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 16 08:45:17 2018

@author: aditya
"""
from kiteconnect import WebSocket
from kiteconnect import KiteConnect
import webbrowser
import openpyxl as data_handler
import time
import logging


#import unicodecsv as csv
from Queue import Queue
import datetime

# local files #
from entities.user import User
from entities.share import Share
from p2vmap import p2v_map as p2v_map
from order_manager import Order_Manager
from olhc_computation_worker import OLHC_Computation_Worker




#Global config variables
timestr = time.strftime("%Y%m%d-%H%M%S")
logger_filename = 'log_' + str(timestr) + '.txt' 
logging.basicConfig(level=logging.DEBUG, format='[%(asctime)s] [%(relativeCreated)6d] [%(levelname)s]  [%(threadName)s] %(message)s', filename=logger_filename)
config_filename =  'deepak.xlsx'
expected_values_sheetname = 'Tech'
user_share_details_sheetname = 'Sheet1'
timestr = time.strftime("%Y%m%d-%H%M%S")
olhc_filename =  'olhc_' + str(timestr)
candlestick_time = 10
result_dir = "results/"



wb = data_handler.load_workbook(config_filename)
user_share_sheet = wb[user_share_details_sheetname]


#read the user_info file
user = User()
response = user.set_access_info(email = user_share_sheet[p2v_map['email']].value, 
                                secret= user_share_sheet[p2v_map['secret']].value, 
                                api_key=user_share_sheet[p2v_map['api_key']].value)

logging.debug('User values read successfully from config file')

#read stock file details
share = Share()
share.quantity = user_share_sheet[p2v_map['share_quantity']].value
share.stop_loss_units = user_share_sheet[p2v_map['stop_loss_units']].value
share.script_absolute_move = user_share_sheet[p2v_map['script_absolute_move']].value
share.time_of_order_execution = user_share_sheet[p2v_map['time_of_order_execution']].value
share.force_square_off_time = user_share_sheet[p2v_map['force_square_off_time']].value
share.position_taken = user_share_sheet[p2v_map['position_taken']].value
share.position_liquidated = user_share_sheet[p2v_map['position_liquidated']].value
share.p_and_l = user_share_sheet[p2v_map['p_and_l']].value
share.last_updated = datetime.datetime.now()
share.instrument = user_share_sheet[p2v_map['instrument_name']].value
share.candlestick_timeframe = candlestick_time

logging.debug('Share values read successfully from config file')

#create a connection object based on the above details
KITE = KiteConnect(api_key= user.access_info['api_key'])
webbrowser.open(KITE.login_url())
print('Enter request token. (please Enclose your response in duble quotes) ')
logging.debug('Awaiting user input')
request_token=input()
logging.debug('User input for request token received')
#response = user.set_access_token("5yc1h6k9jq8xo8rgmp4iyv2n00xi6jsi")

try:
    kite_user = KITE.request_access_token(request_token=request_token, secret=user.access_info['secret'])
    KITE.set_access_token(kite_user["access_token"])
    logging.info('Access token received successfully')
except Exception as e:
        logging.error('Authentication failed : ERROR : %s', str(e))
        raise
logging.info('User %s has logged in successfully', str(kite_user["user_id"]))
user.personal_info['id'] = kite_user["user_id"]

response = user.set_access_token(kite_user["access_token"])
user_share_sheet[p2v_map['access_token']].value = kite_user["access_token"]
wb.save(config_filename)


# Initialise.
#kws = WebSocket("a3gnxpvd9cbcx8ki", "ud386m1jdo27eswwzmivck2iu38iadl8", "YL2856")
kws = WebSocket(user.access_info['api_key'], kite_user["access_token"], user.personal_info['id'])
socketdata_queue = Queue()
olhc_queue = Queue()

# Callback for tick reception.
def on_tick(tick, ws):
    #print(tick)
    timestamp = datetime.datetime.now()
    socketdata_queue.put((timestamp, tick[0]['last_price']), block=False)
    logging.debug(' [on_tick] Tick pushed in the socketdata_queue')
    

# Callback for successful connection.
#Remove the hardcoding
def on_connect(ws):
    #ws.subscribe([738561])
    #logging.info('[on_connect] Subscribed to instrument ID %s successfully', str(738561))
    #ws.set_mode(ws.MODE_LTP, [738561)
    #import pdb; pdb.set_trace()
    ws.subscribe([int(share.instrument)])
    logging.info('[on_connect] Subscribed to instrument ID %s successfully', str(share.instrument))
    ws.set_mode(ws.MODE_LTP, [share.instrument])
    

def on_reconnect(ws, attempts_count):
    #Retries are automatic. 
    logging.warn('[on_reconnect] Socket connection disconnected.Trying to reconnect...Attempt number %s ', str(attempts_count))

def on_noreconnect(ws):
    logging.error('[on_noreconnect] Socket Connection Terminated after multiple reattempts')


def on_error(ws, code, reason):
    logging.error('[on_error] Error occoured in the Socket Connection. [%s] Error : %s', str(code), str(reason))
    
def on_close(ws, code, reason):
    logging.info('[on_close] Socket connection closed successfully')




# Assign the callbacks.
kws.on_tick = on_tick
kws.on_connect = on_connect
logging.debug('Creating OLHC computation worker')
olhc_computation_worker = OLHC_Computation_Worker(socketdata_queue, olhc_queue, candlestick_time, result_dir + olhc_filename) 
#olhc_computation_worker.setDaemon(True)
logging.debug('Creating Order Manager')
order_manager = Order_Manager(olhc_queue, result_dir + olhc_filename, config_filename, expected_values_sheetname)


kws.enable_reconnect(reconnect_interval=5, reconnect_tries=50)
# Infinite loop on the main thread. Nothing after this will run.
# You have to use the pre-defined callbacks to manage subscriptions.
logging.debug('Starting OLHC computation worker')
olhc_computation_worker.start()
logging.debug('Starting Order Manager')
order_manager.start()

kws.connect(threaded=True)

while True:
    socketdata_queue.join()
