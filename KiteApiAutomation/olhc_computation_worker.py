#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 17 11:09:59 2018

@author: aditya
"""


from threading import Thread
import datetime
import unicodecsv as csv
import logging


class OLHC_Computation_Worker(Thread):
    def __init__(self, socketdata_queue, olhc_queue, candlestick_frame, filename):
        Thread.__init__(self, name="OLHC Computation Worker")
        self.socketdata_queue = socketdata_queue
        self.olhc_queue = olhc_queue
        self.candlestick_frame = candlestick_frame
        #To save the OLHC values
        self.filename = filename
        self.start_timestamp = None
        self.latest_olhc = {'open': -1 ,'high':-1, 'low':-1, 'close': -1}
        logging.info('[__init__] OLHC Computation Worker initialized successfully')

    def get_olhc(self, last_price, old_olhc):
        olhc = {'open': -1 ,'high':-1, 'low':-1, 'close': -1}
        if old_olhc['open'] == -1:
            olhc['open'] = last_price
            olhc['high'] = last_price
            olhc['low'] = last_price
        else:
            olhc['open'] = old_olhc['open']
            olhc['high'] = last_price if old_olhc['high'] < last_price else old_olhc['high']
            olhc['low'] = last_price if old_olhc['low'] > last_price else old_olhc['low']

        olhc['close'] = last_price
        return olhc
    
    def write_in_csv(self, data_tuple):
        try:
            with open(self.filename + '.csv', 'a') as olhc_file:
                try:
                    writer = csv.writer(olhc_file, delimiter=',', encoding='utf-8')
                    writer.writerow(data_tuple)
                finally:
                    olhc_file.close()
            logging.info('[write_in_csv] Latest OLHC values writen in the csv file')
        except Exception as e:
            logging.error('[write_in_csv] %s', "Error: {}".format(e.message))

    
    
    def run(self):
        logging.info('[run] Starting OLHC computation worker')
        self.write_in_csv(("open", "close", "high", "low", "end_timestamp"))
        while True:
            # Get the work from the queue and expand the tuple
            #print("OLHC Len : " + str(self.socketdata_queue.qsize()))
            timestamp, last_price = self.socketdata_queue.get()
            logging.info('[run] Pulled latest values from the socketdata_queue')
            self.latest_olhc = self.get_olhc(last_price, self.latest_olhc)
            #print(str(self.latest_olhc))
            
            if self.start_timestamp == None:
                #First value in this interval
                self.start_timestamp = timestamp
                end_timestamp = self.start_timestamp + datetime.timedelta(minutes = self.candlestick_frame)
            
            if timestamp >= end_timestamp:
                #end of candlestik frame
                self.olhc_queue.put(({'olhc' : self.latest_olhc, 
                                 'start_timestamp' : self.start_timestamp, 
                                 'end_timestamp' : end_timestamp}))
                
                logging.info('[run] Candlestick olhc values : %s', str(self.latest_olhc))
                
                self.write_in_csv((self.latest_olhc['open'], self.latest_olhc['close'], self.latest_olhc['high'], self.latest_olhc['low'], str(end_timestamp)))
                self.latest_olhc = {'open': -1 ,'high':-1, 'low':-1, 'close': -1}
                self.start_timestamp = None
                #self.olhc_queue.task_done()